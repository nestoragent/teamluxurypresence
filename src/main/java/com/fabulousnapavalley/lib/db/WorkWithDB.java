package com.fabulousnapavalley.lib.db;


import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.pageFactory.AutotestError;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by velichko-aa on 15.09.2016.
 */
public class WorkWithDB {

    public static void execute (String query, String connectionName) {
        DatabaseConnectionFactory dbFactory = Init.getDatabaseConnectionFactory();
        Connection connection;
        if (null == query || query.isEmpty())
            throw new AutotestError("Query is empty.");

        connection = dbFactory.getConnection(connectionName);
        Statement statement;
        try {
            statement = connection.createStatement();
            statement.execute(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
