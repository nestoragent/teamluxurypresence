package com.fabulousnapavalley.pages.fabulousnapavalley;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.Props;
import com.fabulousnapavalley.lib.pageFactory.ElementTitle;
import com.fabulousnapavalley.lib.pageFactory.PageEntry;
import com.fabulousnapavalley.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Nestor on 18.03.2018.
 */
@PageEntry(title = "Sign up")
public class SignUpPage extends AnyPage{

    @FindBy(css = "form#registerForm input[name='email']")
    @ElementTitle("Email")
    public WebElement inputEmail;

    @FindBy(css = "form#registerForm input[name='name']")
    @ElementTitle("Name")
    public WebElement inputName;

    @FindBy(css = "form#registerForm input[name='password']")
    @ElementTitle("Password")
    public WebElement inputPassword;

    @FindBy(css = "form#registerForm button[type='submit']")
    @ElementTitle("Submit")
    public WebElement buttonSubmit;


    public SignUpPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).until(ExpectedConditions.visibilityOf(inputEmail));
    }

    public void sign_up() throws Exception {
        int randomNum = ThreadLocalRandom.current().nextInt(10000, 999999);
        StringBuilder name = new StringBuilder();
        name.append(Props.get("mail.username")).append(randomNum);
        Init.getStash().put("userName", name);
        StringBuilder email = new StringBuilder();
        email.append(Props.get("mail.username")).append("+").append(randomNum).append("@gmail.com");
        Init.getStash().put("userEmail", email);
        fill_field(inputEmail, email.toString());
        fill_field(inputName, name.toString());
        fill_field(inputPassword, Props.get("user.password"));
        press_button(buttonSubmit);
    }
}
