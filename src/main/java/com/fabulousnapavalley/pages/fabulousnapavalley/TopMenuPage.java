package com.fabulousnapavalley.pages.fabulousnapavalley;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.pageFactory.ElementTitle;
import com.fabulousnapavalley.lib.pageFactory.PageEntry;
import com.fabulousnapavalley.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 18.03.2018.
 */
@PageEntry(title = "Top menu")
public class TopMenuPage extends AnyPage{

    @FindBy(css = "div.header-second-version-container img.logo")
    @ElementTitle("Logo")
    public WebElement imgLogo;

    @FindBy(css = "div.header-second-version-container div.contact-no > span")
    @ElementTitle("Phone number")
    public WebElement textPhoneNumber;

    @FindBy(xpath = "//a[text()='FEATURED LISTINGS']")
    @ElementTitle("Featured listings")
    public WebElement linkFeaturedListings;

    @FindBy(xpath = "//a[text()='HOME SEARCH']")
    @ElementTitle("Home search")
    public WebElement linkHomeSearch;

    @FindBy(xpath = "//a[text()='CONTACT']")
    @ElementTitle("Contact")
    public WebElement linkContact;

    @FindBy(css = "div.header-second-version-container div.mobile-nav > a")
    @ElementTitle("Open left side menu")
    public WebElement linkOpenLeftSideMenu;

    public TopMenuPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).until(ExpectedConditions.visibilityOf(imgLogo));
    }
}
