package com.fabulousnapavalley.pages.fabulousnapavalley;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.pageFactory.ElementTitle;
import com.fabulousnapavalley.lib.pageFactory.PageEntry;
import com.fabulousnapavalley.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 18.03.2018.
 */
@PageEntry(title = "Left side menu")
public class LeftSideMenuPage extends AnyPage {

    @FindBy(css = "div.sidemenu-content a.cross-sign")
    @ElementTitle("Close menu")
    public WebElement linkCloseLeftSideMenu;

    @FindBy(xpath = "//div[@class='sidemenu-content']//span[text()='Log In']")
    @ElementTitle("Log In")
    public WebElement linkLogIn;

    public LeftSideMenuPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.visibilityOf(linkCloseLeftSideMenu));
    }
}
