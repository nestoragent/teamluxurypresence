package com.fabulousnapavalley.pages.fabulousnapavalley;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.pageFactory.ElementTitle;
import com.fabulousnapavalley.lib.pageFactory.PageEntry;
import com.fabulousnapavalley.pages.AnyPage;
import com.fabulousnapavalley.pages.Section;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 18.03.2018.
 */
@PageEntry(title = "Home")
public class HomePage extends AnyPage implements Section {

    @FindBy(css = "div.video-section-basic-container h2")
    @ElementTitle("Header")
    public WebElement homeInitHeader;

    @FindBy(css = "div.video-section-basic-container a")
    @ElementTitle("Search all homes")
    public WebElement linkSearchAllHomes;

    public HomePage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).until(ExpectedConditions.visibilityOf(homeInitHeader));
        checkSection();
    }

    @Override
    public void checkSection() {
        Assert.assertEquals("Header text isn't equals", "YVONNE RICH".toLowerCase(), homeInitHeader.getText().toLowerCase());
        Assert.assertTrue("linkSearchAllHomes element isn't diplayed", linkSearchAllHomes.isDisplayed());
    }
}
