package com.fabulousnapavalley.pages.fabulousnapavalley;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.pageFactory.ElementTitle;
import com.fabulousnapavalley.lib.pageFactory.PageEntry;
import com.fabulousnapavalley.pages.AnyPage;
import com.fabulousnapavalley.pages.Section;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 18.03.2018.
 */
@PageEntry(title = "About")
public class AboutPage extends AnyPage implements Section {

    @FindBy(css = "section.about div.owner")
    @ElementTitle("Image owner")
    public WebElement divImageOwner;

    @FindBy(css = "section.about div.about-content > h4")
    @ElementTitle("Owner name")
    public WebElement headerOwnerName;

    @FindBy(css = "section.about div.about-content > p")
    @ElementTitle("Owner description")
    public WebElement textOwnerDescription;

    @FindBy(css = "section.about div.read-more-wrapper > a")
    @ElementTitle("Read more")
    public WebElement linkReadMore;

    public AboutPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).until(ExpectedConditions.visibilityOf(divImageOwner));
        checkSection();
    }

    @Override
    public void checkSection() {
        scrollTo(divImageOwner);
        Assert.assertTrue("divImageOwner element isn't diplayed", divImageOwner.isDisplayed());
        scrollTo(headerOwnerName);
        Assert.assertEquals("Owner name text isn't equals", "Yvonne Rich".toLowerCase(), headerOwnerName.getText().toLowerCase());

        String descriptionTextPartOne = "Yvonne Rich has been one of the most highly recognized agents in the " +
                "Napa Valley known for her professionalism, insightful local knowledge, and 25+ years of experience " +
                "selling Napa Valley luxury real estate.";
        Assert.assertThat("Owner description isn't contains expected text.",
                textOwnerDescription.getText(), CoreMatchers.containsString(descriptionTextPartOne));

        String descriptionTextPartTwo = "There are ever evolving technology trends that are shaping our industry." +
                " Yvonne Rich Exclusive Estates takes the necessary steps to implement cutting-edge technology to stay" +
                " ahead of the competition. No other company in Napa valley is in a better position to leverage this" +
                " technology to target qualified buyers for your property.";
        Assert.assertThat("Owner description isn't contains expected text.",
                textOwnerDescription.getText(), CoreMatchers.containsString(descriptionTextPartTwo));

        Assert.assertTrue("linkReadMore element isn't diplayed", linkReadMore.isDisplayed());
    }
}
