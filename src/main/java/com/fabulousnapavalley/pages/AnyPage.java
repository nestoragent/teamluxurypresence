package com.fabulousnapavalley.pages;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Set;

/**
 * Created by Nestor on 18.03.2018.
 */
public abstract class AnyPage extends Page {

    public void wait_new_handler(String handlerCount) {
        try {
            int repeatCount = 0;
            while (Init.getDriver().getWindowHandles().size() != Integer.parseInt(handlerCount) && repeatCount < 10) {
                sleep(1);
                repeatCount++;
            }
        } catch (Exception e) {
            System.err.println("Error when wait new handler. Message = " + e.getMessage());
        }
    }

    public void switch_to_the_new_page() throws Exception {
        String newHandler = Init.getDriverExtensions().
                findNewWindowHandle((Set<String>) Init.getStash().get("mainWindowHandle"), Init.getTimeOut());
        Init.getDriver().switchTo().window(newHandler);
        Init.getDriver().switchTo().defaultContent();
        Init.getDriver().switchTo().activeElement();
    }

    public void close_new_page() throws Exception {
        Init.getDriver().close();
        Init.getDriver().switchTo().window(((Set<String>) Init.getStash().get("mainWindowHandle")).iterator().next());
        Init.getDriver().switchTo().defaultContent();
        Init.getDriver().switchTo().activeElement();
    }

    protected WebElement findElementByTextFromList(By list, String text) {
        Init.getDriverExtensions().waitForPageToLoad();
        List<WebElement> items = Init.getDriver().findElements(list);
        return items.stream().filter(el -> el.getText().equalsIgnoreCase(text)).findFirst().orElse(null);
    }

    protected WebElement findElementByTextFromList(By list, By inner, String text) {
        Init.getDriverExtensions().waitForPageToLoad();
        List<WebElement> items = Init.getDriver().findElements(list);
        return items.stream().filter(el -> el.findElement(inner).getText().
                equalsIgnoreCase(text)).findFirst().orElse(null);
    }

}
