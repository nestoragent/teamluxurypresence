package com.fabulousnapavalley.stepDefinitions;

import com.fabulousnapavalley.lib.Init;
import com.fabulousnapavalley.lib.Props;
import com.fabulousnapavalley.lib.util.Excel;
import com.fabulousnapavalley.lib.util.Mail;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;


/**
 * Created by VelichkoAA on 14.01.2016.
 */
public class CommonStepDefinitions {

    @When("^(I|User) is on page \"(.*?)\"$")
    public void init_current_page(String action, String title) throws Throwable {
        Init.getPageFactory().getPage(title);
    }

    @When("^(I|User) \\((.*?)\\) \"([^\"]*)\"$")
    public void standard_action_1(String who, String action, Object param) throws Throwable {
        Init.getPageFactory().getCurrentPage().takeAction(action, param);
    }

    @When("^(I|User) \\((.*?)\\).* \"([^\"]*)\".* \"([^\"]*)\"[^\"]*$")
    public void standard_action_2(String who, String action, String param1, String param2) throws Throwable {
        Init.getPageFactory().getCurrentPage().takeAction(action, param1, param2);
    }

    @When("^(I|User) \\((.*?)\\)$")
    public void standard_action_3(String who, String action) throws Throwable {
        Init.getPageFactory().getCurrentPage().takeAction(action);
    }

    @When("^(I|User) \\((.*?)\\) with data$")
    public void standard_action_4(String who, String action, DataTable dataTable) throws Throwable {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        Init.getPageFactory().getCurrentPage().takeAction(action, data);
    }

    @When("^(I|User) \\((.*?)\\) \"([^\"]*)\" and input data$")
    public void standard_action_5(String who, String action, String param, DataTable dataTable) throws Throwable {
        Map<String, String> data = dataTable.asMap(String.class, String.class);
        Init.getPageFactory().getCurrentPage().takeAction(action, param, data);
    }

    @When("^(I|User) go to the fabulousnapavalley home page$")
    public void goToTheFabulousnapavalleyHomePage(String who)  {
        Init.getDriver().get(Props.get("applications.url"));
        Init.getDriverExtensions().waitForPageToLoad();
    }

    @Then("^(I|User) activate account from email$")
    public void verifyAccount(String who) throws Throwable {
        Mail mail = new Mail();
        String emailText = mail.getMessageText(Props.get("mail.username") + "@gmail.com", Props.get("mail.password"),
                true, "Activate your World");
        String[] elements = emailText.split("\\n");
        String prefix = "", postfix = "";
        Init.getDriver().get(prefix + postfix);
        Init.getDriverExtensions().waitForPageToLoad();
    }

}
