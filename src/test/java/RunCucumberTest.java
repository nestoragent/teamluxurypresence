
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		glue = {"com.fabulousnapavalley.stepDefinitions"},
		features = {"src/test/resources/features/"},
		tags = {"@initial"},
		plugin = {"pretty", "html:target/cucumber-html-report"}
		)
public class RunCucumberTest {
}