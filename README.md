Example of test framework with a technology stack: Java + Selenium + cucumber + Page Objects + step definitions + allure.

#Pre-condition

Requirements to machine:
- java 8
- git
- maven

For Unix need to set right for executing for drivers, ex.:
`chmod -R 777 src/test/resources/webdrivers/linux64/*`

# Execute test

To run test need to execute:  `mvn clean install -DTAGS=test_tag`
ex.: `mvn clean install -DTAGS=signup`

where `-D` - this is how maven send the value to the tests

# Generate report

To generate Allure report:
`mvn site:site`

The report will be generated into the folder: <projectBaseDir>/target/site/

# Configure
Can change the browser here: /src/test/resources/application.properties